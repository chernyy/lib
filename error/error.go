package error

import (
	"fmt"
	"log"
	"os"
)

const (
	DISASTER = "disaster"
	HIGH     = "high"
	AVARAGE  = "avarage"
	WARNING  = "warning"
	INFO     = "information"
	NON      = "not classified"
)

var tempERR = fmt.Sprintf(`### %v ### 
Funciton - (%v)`)

func ErrorLog(category, function string, err error) {
	switch category {
	case DISASTER:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
			os.Exit(901)
		}

	case HIGH:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
			os.Exit(902)
		}
	case AVARAGE:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
			os.Exit(903)
		}
	case WARNING:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
		}
	case INFO:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
		}
	case NON:
		if err != nil {
			log.Printf(`### %v ### 
		Funciton - (%v) "%v"`, category, function, err.Error())
		}
	}
}
