package server

import (
	"crypto/rand"
	"fmt"
	"net/http"
	"time"

	"sync"

	"github.com/go-martini/martini"
)

var mutex = &sync.Mutex{}

const (
	COOKIE_NAME = "sessionId"
)

func GenerateId() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

type Session struct {
	id              string
	Username        string
	LastIdDepatment int
	DepatmentName   string
	IsAuthorized    bool
	AdminSub        map[int]bool
	UserAuthorized  bool
}

type SessionStore struct {
	data map[string]*Session
}

func NewSessionStore() *SessionStore {
	s := new(SessionStore)
	s.data = make(map[string]*Session)
	return s
}

func (store *SessionStore) Get(sessionId string) *Session {
	session := store.data[sessionId]
	if session == nil {
		return &Session{id: sessionId}
	}
	return session
}

func (store *SessionStore) Set(session *Session) {
	store.data[session.id] = session
}

func ensureCookie(r *http.Request, w http.ResponseWriter) string {
	cookie, _ := r.Cookie(COOKIE_NAME)
	if cookie != nil {
		return cookie.Value
	}
	sessionId := GenerateId()

	cookie = &http.Cookie{
		Name:    COOKIE_NAME,
		Value:   sessionId,
		Expires: time.Now().Add(24 * time.Hour),
	}
	http.SetCookie(w, cookie)

	return sessionId
}

var sessionStore = NewSessionStore()

func Middleware(ctx martini.Context, r *http.Request, w http.ResponseWriter) {
	sessionId := ensureCookie(r, w)
	session := sessionStore.Get(sessionId)

	ctx.Map(session)

	ctx.Next()
	mutex.Lock()
	sessionStore.Set(session)
	mutex.Unlock()
}
