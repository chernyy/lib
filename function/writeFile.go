package function

import (
	"gitlab.com/Cherniy/lib/error"
	"io/ioutil"
	"os"
)

func WriteFile(path, name, body string) {
	err := ioutil.WriteFile(path+"/"+name, []byte(body), 0644)
	error.ErrorLog(error.HIGH, "lib - function WriteFile()", err)
}
func CreateFile(path, name, body string) {
	file, err := os.Create(path + "/" + name)
	error.ErrorLog(error.HIGH, "lib - function CreateFile()", err)
	file.Write([]byte(body))
}
